# About the Squad
To read the Markdown files about the squad, open the `source` directory and start with `index.md`.

We recommend to view them in GitLab.com!